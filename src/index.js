const moviesData = require('../dataset/movies.json')

const express = require('express')
const { response } = require('express')
const app = express()

var _ = require('underscore')

app.get('/', function (request, response) {
    response.send('Probando desde API REST')
})

app.get('/movies', function (request, response) {
    response.status(200).json(moviesData)
})

app.get('/movies/:name', (request, response) => {
    let reg = new RegExp(request.params.name)
    let arr = []
    for (let movie of moviesData){
        if (String(movie['Title']).match(reg) ){
            arr.push(movie)
        }
    }
    if(Array.isArray(arr) && arr.length)response.status(200).json(arr);
    else response.status(200).send('No se encontro el titulo buscado');
})

app.get('/movies/rating/:classifier/:order', (request, response) => {
    let sortedMovies = []
    let error = 0;
    //aqui se uso el paquete underscore.js instalado con npm install underscore
    if(request.params.classifier == "rotten"){
        sortedMovies = _.sortBy(moviesData, function(movie) {
            return movie['Rotten Tomatoes Rating']
        });
    }
    else if(request.params.classifier == "imdb"){
        sortedMovies = _.sortBy(moviesData, function(movie) {
            return movie['IMDB Rating']
        });
    }else error++

    if(request.params.order == "asc" && error == 0)response.status(200).json(sortedMovies)
    else if(request.params.order == "desc" && error == 0)response.status(200).json(sortedMovies.reverse())
    else error++

    if(error > 0)response.status(500).send('Hubo un error al procesar los datos, intente nuevamente')
})

app.listen(3000, function (){
    console.log('Server is running in port 3000')
})